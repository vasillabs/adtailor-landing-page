let Utility = {
	random: function (min, max) {
		return Math.floor(Math.random() * (max - min) + min);
	}
};


let AnimateMap = function (map) {
	let self = this;
	this.map = map;
	this.init = function () {
			self.pathIsAnimating = false;
			self.animatingIsFinish = true;
			self.coordinate = {
				DataBaseOut: {
					x: 453,
					y: 240
				},
				point1: {
					x: Utility.random(110, 150),
					y: Utility.random(200, 250)
				},
				point2: {
					x: Utility.random(90, 120),
					y: Utility.random(170, 200)
				},
				point3: {
					x: Utility.random(100, 150),
					y: Utility.random(120, 190)
				},
				DataBaseIn: {
					x: 453,
					y: 240
				},
				PointToUser: {
					x: 450,
					y: 600
				}
			};

			self.count = self.size(self.coordinate);
			self.countMax = 6;

			var mapWidth = parseInt(this.map.width());
			var mapHeight = parseInt(this.map.height());
			self.raphaelContainer = Raphael("containerSVGMAP", "100%", "100%");
			self.raphaelContainer.clear();

			self.circle = [];
			$.each(self.coordinate, function (key, value) {
				self.circle.push(self.raphaelContainer.circle(value.x, value.y, 0).attr({
					fill: "#fff",
					stroke: "none",
					r: 1.5,
					"fill-opacity": 1
				}));
				self.circle.push(self.raphaelContainer.circle(value.x, value.y, 0).attr({
					fill: "#3be6d3",
					stroke: "none",
					r: 1.5,
					"fill-opacity": 1
				}));
			});

			self.path = [];
			self.pathStart = [];
			self.pathAnimate = [];

			var i = 0;
			var lastDest = Object.keys(self.coordinate)[0];
			var value = self.coordinate[0];
			var pointIcrementer = 0;

			while (i < self.countMax) {
				pointIcrementer < self.count - 1 ? pointIcrementer++ : pointIcrementer = 0;
				var key2 = Object.keys(self.coordinate)[pointIcrementer];

				if (key2 != lastDest) {
					var value2 = self.coordinate[key2];
					value = self.coordinate[lastDest];

					self.pathStart.push([
						"M", value.x, value.y,
						'L', value.x, value.y
					]);
					if (i == 5) {
						self.path.push(self.raphaelContainer.path([
							"M", value.x, value.y,
							'L', value.x, value.y
						]).attr({
							stroke: "#1DE9B6",
							"stroke-opacity": 0.1,
							"stroke-width": 20
						}));
					} else if (i == 4) {
						self.path.push(self.raphaelContainer.path([
							"M", value.x, value.y,
							'L', value.x, value.y
						]).attr({
							stroke: "#CDDC39",
							"stroke-opacity": 0.1,
							"stroke-width": 20
						}));
					} else {
						self.path.push(self.raphaelContainer.path([
							"M", value.x, value.y,
							'L', value.x, value.y
						]).attr({
							stroke: "#fff",
							"stroke-opacity": 0.1,
							"stroke-width": 1
						}));
					}

					self.pathAnimate.push([
						[
							"M", value.x, value.y,
							'L', value2.x, value2.y
						], key2
					]);
					i++;
					lastDest = key2;
				}
			}

		},

		self.size = function (obj) {
			var size = 0,
				key;
			for (key in obj) {
				if (obj.hasOwnProperty(key)) size++;
			}
			return size;
		},

		self.index = function (object, key2) {
			var i = 0;
			$.each(object, function (key, value) {
				if (key2 == key) {
					return false;
				}
				i++;
			});
			return i;
		},

		this.waveEffect = function (destination) {
			var index = self.index(self.coordinate, destination);

			self.circle[index * 2].animate({
				r: 15,
				"fill-opacity": 0
			}, 700, ">", function () {
				self.circle[index * 2].attr({
					r: 2,
					"fill-opacity": 1
				});
			});

			setTimeout(function () {
				self.circle[index * 2 + 1].animate({
					r: 15,
					"fill-opacity": 0
				}, 1000, ">", function () {
					self.circle[index * 2 + 1].attr({
						r: 2,
						"fill-opacity": 1
					});
				});
			}, 500);
		},

		this.startAnimatePath = function () {
			self.pathIsAnimating = true;
			self.countPath1 = 0;

			if (self.animatingIsFinish) {
				self.renderPath1();
			}
		},

		this.renderPath1 = function () {
			self.animatingIsFinish = false;

			// Path 1
			self.path[self.countPath1].attr({
				path: self.pathStart[self.countPath1],
				"stroke-opacity": .7
			});
			if (self.countPath1 == 4) {
				$('.intro-animation__fan-icon').addClass('animate');
				setTimeout(function () {
					$('.intro-animation__fan-icon').removeClass('animate');
				}, 500)
			}

			self.path[self.countPath1].animate({
				path: self.pathAnimate[self.countPath1][0]
			}, 450, "<>", function () {
				self.waveEffect(self.pathAnimate[self.countPath1][1]);

				self.path[self.countPath1].animate({
					"stroke-opacity": 0
				}, 400);
				if (self.pathIsAnimating) {
					self.countPath1++;
					if (self.countPath1 >= self.countMax) {
						self.countPath1 = 0;
					}
					self.renderPath1();
				} else {
					self.animatingIsFinish = true;
				}
			});
		};

};



var Modal = (function () {

	function Modal(options) {
		if (options == null) return;
		this.animationType = options.animationType;
		this.init();
	}

	Modal.prototype = {
		init: function () {
			this.triggers = document.querySelectorAll("[data-trigger='open']");
			this.screenHeight = window.innerHeight;
			this.setEvents();
		},

		close: function (modal) {
			this._fadeOut(modal);
		},

		open: function (modal) {
			var self = this
			switch (self.animationType) {
				case 'fade-in':
					break;
				case 'fade-in-top':
					self._fadeIn(modal);
					break;
				default:
					break;
			}
		},

		_fadeOut: function (modal) {
			let modalElm = document.querySelector(modal);
			anime({
				targets: modal,
				opacity: [{
					value: [1, 0],
					duration: 300,
					easing: 'easeOutQuad'
				}]
			});

			setTimeout(function () {
				modalElm.style.zIndex = -1;
			}, 300);
		},

		_fadeIn: function (modal) {
			let self = this;
			let modalElm = document.querySelector(modal);
			let modalBox = modalElm.querySelector(".modal__box");
			let modalBoxHeight = modalBox.clientHeight;
			let calculatedGutter = (self.screenHeight - modalBoxHeight) / 2 + "px";

			modalElm.style.zIndex = 9000;

			anime({
				targets: modal,
				opacity: [{
					value: [0, 1],
					duration: 300,
					easing: 'easeInQuad'
				}]
			});

			anime({
				targets: modalBox,
				delay: 200,
				top: ["15em", `${calculatedGutter}`],
				opacity: [{
					value: [0, 1],
					duration: 300,
					easing: 'easeInQuad'
				}]
			});
		},

		setEvents: function () {
			let self = this;
			let overlay, close_button;

			self.triggers.forEach(trigger => {
				trigger.addEventListener("click", function (e) {
					e.preventDefault();
					let modalID = `#${e.target.dataset.target}`;
					let modalDiv = document.querySelector(modalID);
					self.open(modalID);

					overlay = modalDiv.querySelector(".modal__overlay");
					close_button = modalDiv.querySelector(".modal__close");

					overlay.addEventListener("click", function () {
						self.close(modalID);
					});

					close_button.addEventListener("click", function () {
						self.close(modalID);
					});
				})
			})
		}
	}

	return Modal;
})();



var Accordion = (function () {
	function Accordion(el) {
		if (el === null) return;
		this.accorionButtons = el.querySelectorAll('.toggle-accordion');
		this.init();
	}

	Accordion.prototype = {
		init: function () {
			this.setEvents();
		},
		close: function (accordionItem, icon) {
			accordionItem.style.display = "none";
			accordionItem.previousElementSibling.classList.remove('opened');
			icon.classList.remove('opened');
		},
		open: function (accordionItem, icon) {
			accordionItem.previousElementSibling.classList.add('opened');
			icon.classList.add('opened');
			accordionItem.style.display = "block";
		},
		setEvents: function () {
			let self = this;

			self.accorionButtons.forEach(btn => {
				btn.addEventListener("click", function (e) {
					let isOpened = e.target.classList.contains('opened');
					let accordionItem = e.target.nextElementSibling;
					let icon = e.target.querySelector('.icon');
					!isOpened ? self.open(accordionItem, icon) : self.close(accordionItem, icon);
				})
			})
		}
	}

	return Accordion;
})();



let StickyHeader = (function () {
	function StickyHeader() {
		this.alwaysVisible = false;
		this.sticky = document.querySelector('.sticky-nav');
		this.header = document.querySelector('.header');
		this.headerHeight = this.header.offsetHeight;
	}

	StickyHeader.prototype = {
		init: function () {
			this.setupEvents();
		},

		show: function () {
			let self = this;
			self.sticky.style.display = 'block';
		},

		hide: function () {
			let self = this;
			self.sticky.style.display = 'none';
		},

		alwaysShow: function () {
			let self = this;
			self.alwaysVisible = true;
			self.show();
		},

		setupEvents: function () {
			let self = this;
			if (self.alwaysVisible == true) return;
			window.addEventListener('scroll', function (e) {
				this.scrollY > self.headerHeight ? self.show() : self.hide();
			})

		}
	}

	return StickyHeader;
})();

let SwitchWidget = (function () {
	function SwitchWidget(el, prices) {

		if (el == null) return;

		this.triggerButtons = el.querySelectorAll('.switch-widget__button');
		this.textDiscount = el.querySelector('.switch-widget__text');
		this.priceElements = prices;
		this.annualPrices = ['$99.00', '$299.00', '$599.00'];
		this.monthPrices = ['$108.90', '$328.90', '$658.90'];
		this.init();
	}

	SwitchWidget.prototype = {
		init: function () {
			this.setEvents();
		},
		activateTab: function (elm) {
			elm.classList.add("active");
		},
		deactivateTab: function (elm) {
			let foundSibling = "";
			if (elm.nextElementSibling !== null) {
				foundSibling = elm.nextElementSibling;
			} else if (elm.previousElementSibling !== null) {
				foundSibling = elm.previousElementSibling;
			}
			foundSibling.classList.remove('active');
		},
		changePrice: function (type) {
			var self = this;

			for (let i = 0; i < self.priceElements.length; i++) {
				let element = self.priceElements[i];
				let priceVal = element.querySelector('span');
				let textVal = element.querySelector('small');

				if (priceVal == null || textVal == null) return;

				if (type == 'monthly') {
					priceVal.innerHTML = self.monthPrices[i];
					textVal.innerHTML = '/ per month';
					self.textDiscount.style.textDecoration = "line-through";
					self.textDiscount.style.transform = "scale(0.9, 0.9)";
					self.textDiscount.style.opacity = "0.5";
					self.textDiscount.style.color = "#a9b7bf";
				} else {
					priceVal.innerHTML = self.annualPrices[i];
					textVal.innerHTML = '/ per annual';
					self.textDiscount.style.textDecoration = "none";
					self.textDiscount.style.transform = "scale(1, 1)";
					self.textDiscount.style.opacity = "1";
					self.textDiscount.style.color = "#565656";
					self.textDiscount.style.fontSize = "12px";
				}
			}

		},
		setEvents: function () {
			let self = this;
			self.triggerButtons.forEach(btn => {
				btn.addEventListener("click", function (e) {
					self.deactivateTab(e.target);
					self.activateTab(e.target);

					if (e.target.classList.contains('button-monthly')) {
						self.changePrice('monthly');
					} else {
						self.changePrice('annually');
					}
				})
			})
		}
	}

	return SwitchWidget;
})();


var GMap = (function () {
	function GMap() {
		this.mapContainer = document.getElementById('map'),
			this.locationBtn = document.querySelectorAll(".col-location");

		this.cityCoordinates = {};
		this.cityCoordinates["toronto"] = {
			lat: 43.6706061,
			lng: -79.3903894
		}
		this.cityCoordinates["sofia"] = {
			lat: 42.7007774,
			lng: 23.326452300000028
		}
		this.mapOptions = {
			zoom: 15,
			panControl: true,
			zoomControl: true,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: true,
			rotateControl: false
		}
		this.map = this.init();
	}

	GMap.prototype = {
		init: function () {
			var self = this;
			var map = new google.maps.Map(this.mapContainer, this.mapOptions);

			for (const city in this.cityCoordinates) {
				if (this.cityCoordinates.hasOwnProperty(city)) {
					var marker = new google.maps.Marker({
						position: this.cityCoordinates[city],
						map: map
					});

					var infoWindow = new google.maps.InfoWindow();

					infoWindow.setContent(`<div class="map-wrap">
									<span style="display:block;text-transform: capitalize;padding-bottom:1em;color:#000;font-weight:bold;"> ${city} </span>
									<p class="map-img"><img src="../img/logo-color.png" class="responsive"></p>
						</div>`);
					infoWindow.open(map, marker);


					setTimeout(() => {
						map.setCenter(this.cityCoordinates["toronto"]);
					}, 1000);
				}
			}

			this.setListeners(map);
		},

		changeLocation: function (map, location) {
			map.setCenter(location);
		},

		setListeners: function (map) {
			var self = this;

			this.locationBtn.forEach(btn => {
				btn.addEventListener("click", function () {
					var cityValue = this.dataset.location;
					if (cityValue === "sofia") {
						self.changeLocation(map, self.cityCoordinates["sofia"]);
						map.setZoom(15);
					} else if (cityValue === "toronto") {
						self.changeLocation(map, self.cityCoordinates["toronto"]);
						map.setZoom(15);
					}
				})
			});
		}
	}

	return GMap;
})();


/* INITIALIZE MODULES */
(function () {
	var modulesFactory = {
		modal: function () {
			let modal = new Modal({
				animationType: 'fade-in-top'
			});
		},
		pricingSwitch: function () {
			let switchWidgetElement = document.querySelector('.switch-widget');
			let priceElements = document.querySelectorAll('.pricing-box__price');
			let switchWidget = new SwitchWidget(switchWidgetElement, priceElements);
		},
		stickyNav: function () {
			let indexPage = document.querySelector('.page-index');
			if (indexPage) {
				let stickyNav = new StickyHeader();
				stickyNav.init();
			} else {
				let stickyNav = new StickyHeader();
				stickyNav.alwaysShow();
			}
		},
		accordion: function () {
			let accordionElement = document.querySelector('.accordion');
			let accordion = new Accordion(accordionElement);
		},
		headerAnimation: function () {
			let map = $('#map-interactive');
			let animateMap = new AnimateMap(map);
			$(map).each(function () {
				setTimeout(function () {
					animateMap.init();
					animateMap.startAnimatePath();
				}, 1200)
			});
		},
		fire: function () {
			this.modal();
			this.pricingSwitch();
			this.stickyNav();
			this.accordion();
			this.headerAnimation();
		}

	}

	document.addEventListener("DOMContentLoaded", function () {
		modulesFactory.fire()
	});

})();

function initMap() {
	var gMap = new GMap();
}


/* 
---------------------------------------------------------
|	1) Modal animations 
|	2) Modal content and triggers - setContent
|	3) _build()
|	4) setEvents()
	5) Modal recalculate on resize 
|
|	SECTIONS :: 
|	- Header animation - sliding and changin content
|	- Who we are 
|	- Adtailor brings
|
|	Pricing page      

1) Fix index header sticky
		2) Pricing - animate 
		3) Smooth open accordion

    1) Build js module	
    2) Refactor 
    3) Pop up 
    4) Pricing page
    5) Fixed top
    6) Animate the screen
    7) Heade on slide animte overlay
---------------------------------------------------------
*/