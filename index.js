var static = require('node-static');
var file = new static.Server();
let port = process.env.PORT || 3000;
require('http').createServer(function(request, response) {
  request.addListener('end', function() {
    file.serve(request, response);
  }).resume();
}).listen(port, function() {
    console.log(`The app is running on port ${port}`);
});
